import com.devcamp.jbr260.Animal;
import com.devcamp.jbr260.Cat;
import com.devcamp.jbr260.Dog;
import com.devcamp.jbr260.Mammal;

public class App {
    public static void main(String[] args) throws Exception {
        Animal animal1 = new Animal("Rong");
        Animal animal2 = new Animal("Ky Lan");
        //Subtask 6
        System.out.println(animal1.toString());
        System.out.println(animal2.toString());
        // Subtask 7
        Mammal mammal1 = new Mammal("Khung long");
        Mammal mammal2 = new Mammal("Tiger");
        System.out.println(mammal1.toString());
        System.out.println(mammal2.toString());
        // Subtask 8
        Cat cat1 = new Cat("Mun");
        Cat cat2 = new Cat("Hoa");
        System.out.println(cat1.toString());
        System.out.println(cat2.toString());
       
        // Subtask 9
        Dog dog1 = new Dog("Bull Phap");
        Dog dog2 = new Dog("Alaska");
        System.out.println(dog1.toString());
        System.out.println(dog2.toString());

         // Subtask 10
         cat1.greets();
         cat2.greets();
        // Subtask 11
        dog1.greets();
        dog2.greets();
        // Subtask 12
        dog1.greets(dog2);
    }
}
